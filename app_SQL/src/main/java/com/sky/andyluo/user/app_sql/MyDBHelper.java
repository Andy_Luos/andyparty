package com.sky.andyluo.user.app_sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 『建立資料庫與表格』繼承SQLiteOpenHelper的物件並建立資料庫與表格
 */

public class MyDBHelper extends SQLiteOpenHelper {

    // 資料庫名稱
    public static final String DATABASE_NAME = "mydata.db";

    // 資料庫版本，資料結構改變的時候要更改這個數字，通常是加一
    public static int VERSION = 1;

    // 資料庫物件，固定的欄位變數
    private static SQLiteDatabase database;

    public MyDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override  //只有在資料庫不存在時,才會呼叫onCreate()建立資料庫
    public void onCreate(SQLiteDatabase db) {
/**
 *建立要存放資料的資料表格
 * 1. SQL語法不分大小寫
 * 2. 這裡大寫代表的是SQL標準語法, 小寫字是資料表/欄位的命名建立應用程式需要的表格
 */

        db.execSQL(Test_Data.CREATE_TABLE);

    }

    @Override  ////使用建構子時如果版本增加,便會呼叫onUpgrade()刪除舊的資料表與其內容,再重新呼叫onCreate()建立新的資料表
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Test_Data.TABLE_NAME);
        // 呼叫onCreate建立新版的表格
        onCreate(db);
    }

    // 需要資料庫的元件呼叫這個方法，這個方法在一般的應用都不需要修改
    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {

            //這邊『非常的重要』SQL 2大主軸(SQLiteOpenHelper，SQLiteDatabase)
            //1.先初始化 繼承SQLiteOpenHelper的物件
            MyDBHelper myDBHelper = new MyDBHelper(context, DATABASE_NAME, null, VERSION);

            //2.再從此物件利用getWritableDatabase()的方法  取得SQLiteDatabase
            database = myDBHelper.getWritableDatabase();
        }
        return database;
    }

    @Override //SQLiteOpenHelper
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override //SQLiteOpenHelper
    public synchronized void close() {
        super.close();
    }

    @Override //Java
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
