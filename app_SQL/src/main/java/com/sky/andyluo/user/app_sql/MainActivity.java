package com.sky.andyluo.user.app_sql;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 *
 * SQL
 * 主要2個類：SQLiteOpenHelper、SQLiteDatabase
 *
 * 1.MyDBHelper
 * 　(A)　創建一個類 繼承SQLiteOpenHelper 並實作onCreate、onUpgrade2個方法 『方法再獨立一個類別寫』
 *   (B)　創建資料庫名稱(???.db)、資料庫版本(1 or 2 or 3)、SQLiteDatabase得參考物件
 *   (C)  封裝的概念 通過getter() 讓使用者取得SQLiteDatabase得參考物件
 *   (D) 並且在此 getter()的方法中，可以直接initial MyDBHelper此類別。
 *
 * 2.Item
 *   (A)  創建一個資料表的物件，再全域變數宣告＂所需要的變數＂，再利用getter &setter封裝
 *
 *
 * 3.Test_Data
 *   請看 Test_Data的類別
 */

public class MainActivity extends AppCompatActivity {
    private static MyLogDebug debug;
    private Context context;
    private TextView tv_msg, tv_title;
    private EditText et_id, et_name, et_content, et_number, et_load, et_change, et_delete;
    private LinearLayout screen;
    Test_Data mySQL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        debug = new MyLogDebug();
        debug.log_d("MainActivity onCreate()");

        init();
        startWork();

        //設定進入 app時 見盤不會立馬跳出
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void init() {


        tv_msg = (TextView) findViewById(R.id.tv_msg);
        tv_title = (TextView) findViewById(R.id.tv_title);

        et_id = (EditText) findViewById(R.id.et_id);
        et_name = (EditText) findViewById(R.id.et_name);
        et_content = (EditText) findViewById(R.id.et_content);
        et_number = (EditText) findViewById(R.id.et_number);

        et_load = (EditText) findViewById(R.id.et_load);
        et_change = (EditText) findViewById(R.id.et_change);
        et_delete = (EditText) findViewById(R.id.et_delete);

        screen = (LinearLayout) findViewById(R.id.screen);
    }


    private void startWork() {

        screen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                debug.log_w("FirstFragment screen onClick()");

                //取得焦點 (代表其他地方消失焦點)
                screen.requestFocus();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

        });
    }


    public void onClick(View view) {
        //
        TextView tv_btn = (TextView) view;
        mySQL = new Test_Data(context);


        check_data(view);

        debug.log_w("flag= " + flag);
        if (flag == false) {
            Toast.makeText(context, "尚未輸入資訊", Toast.LENGTH_SHORT).show();
            return;
        }
        ;

        switch (view.getId()) {

            case R.id.button1:   //新增

                //1.自定義寫死的新增資料方法
                mySQL.sample();

                //2.從Edtext中抓取新增的資料
//                Item item = new Item(
//                        Integer.valueOf(et_id.getText().toString()),
//                        et_name.getText().toString(),
//                        et_content.getText().toString(),
//                        Integer.valueOf(et_number.getText().toString()));
//
//                mySQL.insert(item);
                break;


            case R.id.button2:   //修改

                //創立一個資料表的物件，Data並從Edtext中抓取
                Item change_item = new Item(
                        Integer.valueOf(et_id.getText().toString()),
                        et_name.getText().toString(),
                        et_content.getText().toString(),
                        Integer.valueOf(et_number.getText().toString()));

                //自定義修改資料的方法
                mySQL.updata(change_item);

                //把Layout輸入的清除
                et_id.setText("");
                et_name.setText("");
                et_content.setText("");
                et_number.setText("");
                break;

            case R.id.button3:   //刪除

                //從Edtext中取得資料
                int delete_int = Integer.valueOf(et_delete.getText().toString());

                //自定義刪除資料的方法
                mySQL.delete(delete_int);

                //把Layout輸入的清除
                et_delete.setText("");
                break;

            case R.id.button4:   //全部查詢

                //每次查詢 都先將資料清空 再做查詢
                tv_msg.setText("");

                //創建一個 資料表的集合 = 自定義取得資料的方法
                List<Item> list = mySQL.getAll_Item();
                tv_title.setText("總共" + list.size() + "筆");

                for (int i = 0; i < list.size(); i++) {
                    String msg_all = list.get(i).toString();
                    tv_msg.append(msg_all + "\n");
                }
                break;

            case R.id.button5:   //指定查詢

                //從Edtext中取得資料
                int check_load = Integer.valueOf(et_load.getText().toString());

                //自定義指定查詢 資料的方法
                Item item = mySQL.get_oneData(check_load);

                //每次查詢 都先將資料清空 再做查詢
                tv_msg.setText(item + "");

                //把Layout輸入的清除
                et_load.setText("");
                break;

            case R.id.button6:   //筆數
                int count = mySQL.getCount();
                tv_title.setText("總共 " + count + "筆資料");
                break;

        }
        Toast.makeText(context, tv_btn.getText().toString() + "", Toast.LENGTH_SHORT).show();
        mySQL.close();

    }

    private boolean flag;

    private void check_data(View view) {
        debug.log_w("check_data");
        flag = false;


        switch (view.getId()) {

            case R.id.button2:
                debug.log_w("button2");
                if (et_id.getText().length() > 0 &&
                        et_name.getText().length() > 0 &&
                        et_content.getText().length() > 0 &&
                        et_number.getText().length() > 0) {
                    flag = true;
                }
                break;

            case R.id.button3:
                debug.log_w("button3");
                if (et_delete.getText().length() > 0) {
                    debug.log_e("et_delete.getText().toString()= " + et_delete.getText().toString());
                    flag = true;
                }
                break;

            case R.id.button5:
                debug.log_w("button5");
                if (et_load.getText().length() > 0) {
                    debug.log_e("et_load.getText().toString()= " + et_load.getText().toString());
                    flag = true;
                }
                break;

            default:
                flag = true;

        }

    }
}
