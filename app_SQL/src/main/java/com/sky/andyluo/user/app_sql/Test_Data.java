package com.sky.andyluo.user.app_sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * 來實作表單、實作資料庫物件、關閉資料庫、新增記錄、查詢記錄、更改記錄、刪除記錄、建立範例資料
 *
 * 1.先用final 定義欄位的名稱 (物件變數有什麼 就定義)
 *
 * 2.宣告SQL的指令，
 * XXXX = "CREATE TABLE" + 表格名稱 + "(" + 欄位名稱.1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + 欄位名稱.2 + " SQL語法" + ")"
 *
 * 3.需要SQLiteDatabase的物件 就利用MyDBHelper的Getter()方法取得，感覺控制著SQLiteOpenHelper
 *   ex： db = MyDBHelper.getDatabase(context)
 *
 * 4.方法重點：
 * (A) 關閉資料庫→  db.close();
 * (B) 刪除-資料→   db.delete();
 * (C) 新增-資料→   db.insert();
 * (D) 修改-資料→   db.update();
 * (E) 讀取-資料→   db.query();
 * (F) 查詢-筆數→   db.rawQuery();
 *
 * 5.詳細介紹：
   (C) 新增-資料→   方法 = db.insert()，型別 = long
        參數 = (資料表名稱，指定欄位，ContentValues)
        邏輯 = 創立資料後(Item物件)，再由ContentValues取出 等資料
        --------------------------------------------------
        資料表名稱 = TABLE_NAME。
        指定欄位 = null (會幫你自動排序)
        ContentValues = 儲存資料的 Map(key , value)
        →→再把返回的long值 設定進id。
        --------------------------------------------------

   (D) 修改-資料→   方法 = db.update()，型別 = boolean
        參數 = (資料表名稱，ContentValues，where，whereArgs)
        邏輯 =  創立資料後(Item物件)，再由ContentValues取出 等資料 且
                where中的ItemId 不可以自己手動設定+指定，必須從資料取出id
        --------------------------------------------------
        資料表名稱 = TABLE_NAME
        ContentValues = 儲存資料的 Map(key , value)
        where = String where = _KEY_ID + "=" + ItemId; (限制式。)
        whereArgs:where = 限制式對應之?參數內容
        →→返回的boolean值 >0 為true。
        --------------------------------------------------

   (B) 刪除-資料→   方法 = db.delete()，型別 = boolean
        參數 = (資料表名稱，where，whereArgs)
        邏輯 = 不需創立資料，所以也不需ContentValues類別，只需要
                where中的ItemId 為自己手動設定 or 指定
        --------------------------------------------------
        資料表名稱 = TABLE_NAME
        where = String where = _KEY_ID + "=" + item.指定編號;(限制式。)
        whereArgs:where = 限制式對應之?參數內容
        →→返回的boolean值 >0 為true。
        --------------------------------------------------

   (E) 讀取-全部資料→   方法 = db.query()，型別 = Cursor
        參數(資料表名稱，資料數組名稱，条件字句，where，whereArgs，参数数组，分组列，分组条件，排序列)
        邏輯 = 『查全部』所以不需要where，也不需創立資料，所以也不需ContentValues類別，
                只需要迴圈循環 並用到 Cursor(指標)來查詢

        範例： Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

                while (cursor.moveToNext()) {
                    Item myItem = new Item(cursor.getLong(0),cursor.getString(1),cursor.getString(2),cursor.getLong(3));
                    mylist.add(myItem);  }   return mylist

        說明： cursor.moveToNext() 是一個boolean，當true的時候才會執行迴圈
                cursor.moveToNext() 是一個方法 或自動換到下一個指標
                利用cursor.getLong(i) 取得第i個欄位的值，並放入物件，再把物件新增至 List<物件>中
         --------------------------------------------------


        讀取-全部資料→   方法 = db.query()，型別 = Cursor
        參數(資料表名稱，資料數組名稱，条件字句，where，whereArgs，参数数组，分组列，分组条件，排序列)
        邏輯 = 『查指定』所以需要where，不需創立資料，不需ContentValues類別，不需要迴圈循環
                只約要用到 Cursor(指標)來查詢 且 where中的ItemId 為自己手動設定 or 指定

        範例： Cursor cursor = db.query(TABLE_NAME, null, where, null, null, null, null, null);

                if (cursor.moveToFirst()) {
                    item = new Item(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getLong(3));
                                            }   return item

 */


public class Test_Data {

    // 表格名稱
    public static final String TABLE_NAME = "test_data";

    // 編號表格欄位名稱，固定不變
    public static final String _KEY_ID = "_id";
    public static final String NAME_COLUMN = "name";
    public static final String CONTENT_COLUMN = "content";
    public static final String NUMBER_COLUMN = "number";



    // 使用上面宣告的變數建立表格的SQL指令
    // NOT NULL 在沒有做出任何限制的情況下，一個欄位是允許有 NULL 值得。如果我們不允許一個欄位含有 NULL 值，我們就需要對那個欄位做出 NOT NULL 的指定。
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            " (" +
            _KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NAME_COLUMN + " TEXT NOT NULL, " +
            CONTENT_COLUMN + " TEXT, " +
            NUMBER_COLUMN + " INTEGER NOT NULL)";
    //資料庫物件
    private SQLiteDatabase db;

    //《創建》 (建構子 跟繼承SQLiteOpenHelper的物件拿取 SQLiteDatabase)
    public Test_Data(Context context) {
        db = MyDBHelper.getDatabase(context);

    }

    //《關閉資料庫》
    public void close() {
        db.close();
    }


    //《新增》
    public Item insert(Item item) {

        //存取數據的類別，其實就是一個Map
        // 參數(key， Value) ，Value 從物件Item中由getter取得
        ContentValues cv = new ContentValues();
        cv.put(NAME_COLUMN, item.getName());
        cv.put(CONTENT_COLUMN, item.getContent());
        cv.put(NUMBER_COLUMN, item.getNumber());


        // 新增資料的方法 insert。
        // insert(資料表名稱，指定欄位，ContentValues)
        long id = db.insert(TABLE_NAME, null, cv);
        item.setId(id);
        Log.w("tag", "======================");
        Log.d("tag", "id= " + item.getId());
        Log.d("tag", "name= " + item.getName());
        Log.d("tag", "content= " + item.getContent());
        Log.d("tag", "number= " + item.getNumber());
        Log.w("tag", "======================");
        return item;
    }

    //《修改》
    public boolean updata(Item item) {

        //存取數據的類別 (key， Value) ，Value 從物件Item中由getter取得
        ContentValues cv = new ContentValues();
        cv.put(NAME_COLUMN, item.getName());
        cv.put(CONTENT_COLUMN, item.getContent());
        cv.put(NUMBER_COLUMN, item.getNumber());

        Log.w("tag", "======================");
        Log.d("tag", "id= " + item.getId());
        Log.d("tag", "name= " + item.getName());
        Log.d("tag", "content= " + item.getContent());
        Log.d("tag", "number= " + item.getNumber());


        //db.update("person", values, "personid=?", new String{"1"});

        //設定條件為編號，格式為「欄位名稱=資料」
        String where = _KEY_ID + "=" + item.getId();
        Log.w("tag", "updata_where= " + where);
        Log.w("tag", "======================");

        //(Table name，ContentValues，where，???)
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }


    //《刪除》
    public boolean delete(long id) {

        //設定條件為編號，格式為「欄位名稱=資料」
        String where = _KEY_ID + "=" + id;
        Log.w("tag", "delete_where= " + where);
        Log.w("tag", "======================");

        //(Table name，ContentValues，where，???)
        return db.delete(TABLE_NAME, where, null) > 0;
    }


    //《讀取》 需要用到指標 Cursor
    public List<Item> getAll_Item() {

        List<Item> mylist = new ArrayList<>();


        /*
         *  query 參數大集合(
         *    String table,     資料表名稱
         *    String[] columns, 資料數組名稱
         *    ------------這2組DATA息息相關，範例中 "Test_Data.NAME_COLUMN = ?" →玩家1號
         *    ------------這2組DATA息息相關，範例中 "OR Test_Data.NAME_COLUMN = ?" →玩家4號
         *    String selection,         条件字句，where (指定位置)
         *    String[] selectionArgs,   条件字句，参数数组
         *    -----------------------------------
         *    String groupBy,  分组列
         *    String having,  分组条件
         *    String orderBy  排序列 <ASC 代表結果會以由小往大的順序列出，而 DESC 代表結果會以由大往小的順序列出。>
         *                           )
         */



        //資料表名稱
        String tableName = TABLE_NAME;

        //預查詢的數組名稱
        String[] columns = {Test_Data._KEY_ID, Test_Data.NAME_COLUMN};

        //查詢的目的 [數組的目的]
        String selection = Test_Data.NAME_COLUMN +" = ? OR " +Test_Data.NAME_COLUMN + " = ? ";

        //查詢目的的 [數組目的]
        String[] selectionArgs = {"玩家1號","玩家4號"};

        Cursor cursor = db.query(tableName,
                                columns,
                                selection,
                                selectionArgs,
                                 null,
                                 null,
                                 _KEY_ID+" ASC");

//        while (cursor.moveToNext()) {
//            Log.w("tag", "======================");
//            Log.d("tag", "cursor= " + cursor);
//            Log.d("tag", "cursor.getPosition()= " + cursor.getPosition());
//
//            Item myItem = new Item(cursor.getLong(0),cursor.getString(1),cursor.getString(2),cursor.getLong(3));
//            mylist.add(myItem);
//
//            Log.d("tag", "id= "+cursor.getLong(0));
//            Log.d("tag", "name= "+cursor.getString(1));
//            Log.d("tag", "content= "+cursor.getString(2));
//            Log.d("tag", "number= "+cursor.getLong(3));
//
//        };
//        Log.w("tag", "======================");

//方法2=============================================================================================
            Log.w("tag", "===========cursor.getCount()==========="+cursor.getCount());
            cursor.moveToFirst();
            do {
                try {
                    Log.w("tag", "======================");
                    Log.d("tag", "cursor= " + cursor);
                    Log.d("tag", "cursor.getPosition()= " + cursor.getPosition());


                    //指定型別
//                    Item myItem = new Item(
//                            cursor.getLong(0),
//                            cursor.getString(1),
//                            cursor.getString(2),
//                            cursor.getLong(3));

                    //指定型別 再指定對應的欄位
                    Item myItem = new Item(
                            cursor.getLong  (cursor.getColumnIndex(_KEY_ID)),
                            cursor.getString(cursor.getColumnIndex(NAME_COLUMN))

                    );


                    mylist.add(myItem);

                    Log.d("tag", "id= " + cursor.getLong(0));
                    Log.d("tag", "name= " + cursor.getString(1));
//                    Log.d("tag", "content= " + cursor.getString(2));
//                    Log.d("tag", "number= " + cursor.getLong(3));
                }catch (Exception e){
                    Log.e("tag", "error= " + cursor.getPosition());
                }

            } while (cursor.moveToNext());

            Log.w("tag", "======================");



//方法3=============================================================================================
//            for (int i = 1; i <= max; i++) {
//
//                cur.moveToNext();
//
//                if(cursor.getCount()<=cursor.getPosition()){break;}
//
//                Log.w("tag", "===========max===========" + max);
//                Log.d("tag", "cursor= " + cursor);
//                Log.d("tag", "cursor.getPosition()= " + cursor.getPosition());
//
//                Item myItem = new Item(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getLong(3));
//                mylist.add(myItem);
//
//                Log.d("tag", "id= " + cursor.getLong(0));
//                Log.d("tag", "name= " + cursor.getString(1));
//                Log.d("tag", "content= " + cursor.getString(2));
//                Log.d("tag", "number= " + cursor.getLong(3));
//            }

    cursor.close();
    return mylist;
}

    // 取得指定編號的資料物件
    public Item get_oneData(long id) {

        String where = _KEY_ID + "=" + id;

        Cursor cursor = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        Item item = null;
        if (cursor.moveToFirst()) {
            // 讀取包裝一筆資料的物件
//            item = getRecord(result);
             item = new Item(
                     cursor.getLong(0),
                     cursor.getString(1),
                     cursor.getString(2),
                     cursor.getLong(3));

        }



        Log.w("tag", "======================");
        Log.d("tag", "cursor= " + cursor);
        Log.d("tag", "cursor.getPosition()= " + cursor.getPosition());
//        Log.d("tag", "id= " + cursor.getLong(0));
//        Log.d("tag", "name= " + cursor.getString(1));
//        Log.d("tag", "content= " + cursor.getString(2));
//        Log.d("tag", "number= " + cursor.getLong(3));
        Log.w("tag", "======================");

        return item;
    }

    //取得資料筆數
    public int getCount() {
        int count = 0;

        //????
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);

        if (cursor.moveToNext()) {
            count = cursor.getInt(0);

            Log.w("tag", "cursor= " + cursor);
            Log.w("tag", "cursor.getInt(0)= " + cursor.getInt(0));
        }
        return count;

    }

    // 建立範例資料
    public void sample() {
        Item item = new Item(0,  "玩家1號", "111", 1);
        Item item2 = new Item(0, "玩家2號", "222", 2);
        Item item3 = new Item(0, "玩家3號", "333", 3);
        Item item4 = new Item(0, "玩家4號", "444", 4);
        insert(item);
        insert(item2);
        insert(item3);
        insert(item4);
    }


}
