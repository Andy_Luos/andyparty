package com.sky.andyluo.user.app_sql;

/**
 * Created by user on 2016/11/1.
 */

//『物件』針對各個欄位的資料寫一個物件方便存取
public class Item {

    private long id;
    private String name;
    private String content;
    private long number;

    public Item() {
    }

    public Item(long id, String name, String content, long number) {
        this.id = id;
        this.name = name;
        this.content = content;
        this.number = number;
    }

    public Item(long id, String name) {
        this.id = id;
        this.name = name;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", number=" + number +
                '}';
    }
}
