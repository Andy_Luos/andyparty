package com.sky.andyluo.user.app_sql;

import android.util.Log;

/**
 * Created by user on 2016/10/19.
 */

public class MyLogDebug {
    private  final String TAG = "tag";
    private  boolean action =true ;

    public boolean isAction() {
        return action;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    public  void log_d(Object msg1) {

        if (action) {
            try {
                Log.d(TAG, msg1.toString());
            } catch (Exception e) {

                Log.d(TAG, "格式錯誤 請檢查");
            }

        }
    }

    public  void log_d(Object msg1, Object msg2) {

        if (action) {
            try {
                Log.d(TAG, msg1.toString() + "=  " + msg2.toString());
            } catch (Exception e) {

                Log.d(TAG, "格式錯誤 請檢查");
            }

        }
    }

    //

    public  void log_e(Object msg1) {

        if (action) {
            try {
                Log.e(TAG, msg1.toString());
            } catch (Exception e) {
                Log.e(TAG, "格式錯誤 請檢查");

            }

        }
    }

    public  void log_e(Object msg1, Object msg2) {

        if (action) {
            try {
                Log.e(TAG, msg1.toString() + "=  " + msg2.toString());
            } catch (Exception e) {

                Log.e(TAG, "格式錯誤 請檢查");
            }

        }
    }

    //

    public  void log_w(Object msg1) {

        if (action) {
            try {
                Log.w(TAG, msg1.toString());
            } catch (Exception e) {
                Log.w(TAG, "格式錯誤 請檢查");

            }

        }
    }

    public  void log_w(Object msg1, Object msg2) {

        if (action) {
            try {
                Log.w(TAG, msg1.toString() + "=  " + msg2.toString());
            } catch (Exception e) {

                Log.w(TAG, "格式錯誤 請檢查");
            }

        }
    }

    //

    public  void log_i(Object msg1) {

        if (action) {
            try {
                Log.i(TAG, msg1.toString());
            } catch (Exception e) {
                Log.i(TAG, "格式錯誤 請檢查");

            }

        }
    }

    public  void log_i(Object msg1, Object msg2) {

        if (action) {
            try {
                Log.i(TAG, msg1.toString() + "=  " + msg2.toString());
            } catch (Exception e) {

                Log.i(TAG, "格式錯誤 請檢查");
            }

        }
    }



}
